Simple script to control ThinkPad X200 display brightness

Usage:
```bash
# cat ~/.xbindkeysrc
"sudo /usr/local/sbin/brightness-control.rb 100"
        XF86MonBrightnessUp

"sudo /usr/local/sbin/brightness-control.rb -100"
        XF86MonBrightnessDown

```

