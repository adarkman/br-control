#! /usr/bin/ruby
# ThinkPad X220 display brightness control
# 

MIN_BR=1000
BR_DELTA=100

BR_CONTROL_NAME='/sys/class/backlight/intel_backlight/brightness'
BR_MAX_NAME='/sys/class/backlight/intel_backlight/max_brightness'

if ARGV.length != 1
  print "Usage: brightness-control.rb <delta>\n"
  exit 0
else
  delta = ARGV[0].to_i
end

current = File.read(BR_CONTROL_NAME).to_i
max = File.read(BR_MAX_NAME).to_i

current += delta
current = MIN_BR if current < MIN_BR
current = max if current > max

File.write BR_CONTROL_NAME, "#{current.to_s}\n"



